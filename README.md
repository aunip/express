# All U Need Is Pizza

> _Made With **NodeJS** 12_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- src
    +-- controllers
        +-- pizzaCtrl.js
    +-- models
        +-- pizzaModel.js
    +-- utils
        +-- index.js
    +-- main.js
    +-- pizzas.json
    +-- webServer.js
+-- .gitignore
+-- LICENSE
+-- package.json
+-- README.md
+-- yarn.lock
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/express.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

### Requirement

- [x] **MongoDB** Server

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
