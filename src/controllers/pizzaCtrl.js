const pizzaModel = require('../models/pizzaModel');
const { isFileExist, readJsonFile, isDefined, bsonToJson } = require('../utils');

/**
 * Initialize Pizzas
 * Read JSON & Feed DB
 */
const initPizzas = () => {
  if (isFileExist('pizzas.json')) {
    const pizzas = readJsonFile('pizzas.json');

    pizzas.forEach(pizza => {
      const { label, items, price } = pizza;

      pizzaModel
        .findOne({ label })
        .then(result => {
          if (!result) {
            pizzaModel.create(pizza).catch(err => {
              console.log(err);
            });
          } else {
            result.items = items;
            result.price = price;

            result.save(err => {
              if (isDefined(err)) {
                console.log(err);
              }
            });
          }
        })
        .catch(err => {
          console.log(err);
        });
    });
  }
};

/**
 * Create Many Pizzas (From Array)
 * REST Method: POST
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const createManyPizzas = ({ body }, res) => {
  if (!isDefined(body)) {
    return res.status(400).send();
  }

  pizzaModel
    .insertMany(body)
    .then(results => {
      return res.status(201).send({ createdCount: results.length });
    })
    .catch(err => {
      return res.status(500).send(err);
    });
};

/**
 * Create Pizza (From Object)
 * REST Method: POST
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const createPizza = ({ body }, res) => {
  const { label, items, price } = body;

  if (!isDefined(label) || !isDefined(items) || !isDefined(price)) {
    return res.status(400).send();
  }

  pizzaModel
    .create({
      label,
      items,
      price
    })
    .then(result => {
      return res.status(201).send({ createdId: bsonToJson(result).id });
    })
    .catch(err => {
      return res.status(500).send(err);
    });
};

/**
 * Get All Pizzas
 * REST Method: GET
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const getAllPizzas = (_, res) => {
  pizzaModel
    .find({})
    .then(results => {
      return res.status(200).send(results.map(result => bsonToJson(result)));
    })
    .catch(err => {
      return res.status(500).send(err);
    });
};

/**
 * Get Pizza (With ID)
 * REST Method: GET
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const getPizza = ({ params }, res) => {
  const { id } = params;

  if (!isDefined(id)) {
    return res.status(400).send();
  }

  pizzaModel
    .findById(id)
    .then(result => {
      if (result) {
        return res.status(200).send(bsonToJson(result));
      }

      return res.status(404).send();
    })
    .catch(err => {
      return res.status(500).send(err);
    });
};

/**
 * Update Pizza (With ID)
 * REST Method: PUT
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const updatePizza = ({ params, body }, res) => {
  const { id } = params;

  if (!isDefined(id)) {
    return res.status(400).send();
  }

  pizzaModel
    .findByIdAndUpdate(id, body)
    .then(result => {
      if (result) {
        return res.status(200).send({ updatedId: bsonToJson(result).id });
      }

      return res.status(404).send();
    })
    .catch(err => {
      return res.status(500).send(err);
    });
};

/**
 * Clear All Pizzas
 * REST Method: DELETE
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const clearAllPizzas = (_, res) => {
  pizzaModel
    .deleteMany({})
    .then(results => {
      return res.status(200).send({ deletedCount: results.deletedCount });
    })
    .catch(err => {
      return res.status(500).send(err);
    });
};

/**
 * Clear Pizza
 * REST Method: DELETE
 *
 * @param {Object} req Request
 * @param {Object} res Response
 */
const clearPizza = ({ params }, res) => {
  const { id } = params;

  if (!isDefined(id)) {
    return res.status(400).send();
  }

  pizzaModel
    .findByIdAndDelete(id)
    .then(result => {
      if (result) {
        return res.status(200).send({ deletedId: bsonToJson(result).id });
      }

      return res.status(404).send();
    })
    .catch(err => {
      return res.status(500).send(err);
    });
};

module.exports = {
  initPizzas,
  createManyPizzas,
  createPizza,
  getAllPizzas,
  getPizza,
  updatePizza,
  clearAllPizzas,
  clearPizza
};
