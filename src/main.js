const chalk = require('chalk');
const mongoose = require('mongoose');
const webServer = require('./webServer');
const { initPizzas } = require('./controllers/pizzaCtrl');

const app = webServer();

// MongoDB Config
const { host, port, db } = {
  host: 'localhost',
  port: 27017,
  db: 'aunip'
};

const uri = `mongodb://${host}:${port}/${db}`;

// MongoDB Instance
mongoose
  .connect(uri, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    initPizzas();
    app.launchServer();
    console.log(`[DB] ${chalk.green('Success !')}`);
  })
  .catch(() => {
    console.log(`[DB] ${chalk.red('Failed...')}`);
  });
