const { accessSync, readFileSync } = require('fs');
const { resolve } = require('path');

/**
 * Is Defined
 *
 * @param {Any} data Data
 * @returns {Boolean} Result
 */
const isDefined = data => {
  if (data === undefined || data === null) {
    return false;
  }

  return true;
};

/**
 * BSON To JSON
 *
 * @param {Object} document Document
 * @returns {Object} Formatted Document
 */
const bsonToJson = ({ _doc: doc }) => {
  const { _id: id, _class, ...data } = doc;

  return {
    id,
    ...data
  };
};

/**
 * Is File Exist
 *
 * @param {String} fileName FileName
 * @returns {Boolean} Result
 */
const isFileExist = fileName => {
  try {
    accessSync(resolve(__dirname, '..', fileName));
  } catch (e) {
    // console.log(e);
    return false;
  }

  return true;
};

/**
 * Read JSON File
 *
 * @param {String} fileName FileName
 * @returns {Any} JSON
 */
const readJsonFile = fileName => {
  const content = readFileSync(resolve(__dirname, '..', fileName));

  return JSON.parse(content);
};

module.exports = {
  isDefined,
  bsonToJson,
  isFileExist,
  readJsonFile
};
