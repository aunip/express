const express = require('express');
const cors = require('cors');
const { json } = require('body-parser');
const { serve, setup } = require('swagger-ui-express');
const { createManyPizzas, createPizza, getAllPizzas, getPizza, updatePizza, clearAllPizzas, clearPizza } = require('./controllers/pizzaCtrl');
const swagger = require('./swagger.json');

/**
 * Web Server
 */
function webServer() {
  const server = express();
  const port = 5050;

  applyMiddleware();
  mountRoutes();

  /**
   * Apply Middleware
   * CORS / JSON / URL Encode
   */
  function applyMiddleware() {
    server.use(cors());
    server.use(json());
  }

  /**
   * Mount Routes
   * Pizza(s) API
   */
  function mountRoutes() {
    server.post('/api/pizza', createPizza);

    server
      .route('/api/pizzas')
      .post(createManyPizzas)
      .get(getAllPizzas)
      .delete(clearAllPizzas);

    server
      .route('/api/pizza/:id')
      .get(getPizza)
      .put(updatePizza)
      .delete(clearPizza);

    server.use('/api-docs', serve, setup(swagger));
  }

  /**
   * Launch Server
   * Display...
   */
  function launchServer() {
    server.listen(port, () => {
      console.log(`> Listening On 'http://localhost:${port}'`);
    });
  }

  return {
    launchServer
  };
}

module.exports = webServer;
